import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReferencesinfoComponent } from './referencesinfo.component';

describe('ReferencesinfoComponent', () => {
  let component: ReferencesinfoComponent;
  let fixture: ComponentFixture<ReferencesinfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReferencesinfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReferencesinfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
