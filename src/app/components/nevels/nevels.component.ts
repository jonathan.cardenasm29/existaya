import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-nevels',
  templateUrl: './nevels.component.html',
  styleUrls: ['./nevels.component.css']
})
export class NevelsComponent implements OnInit {
  public nevels: any[] = [
    {name: 'Nivel 1 al 4', id: 'nivel1', modules: [
                                {id: 'fund1', name: 'fundamental', text: 'text', image: 'n1f1.JPG',
                                  features: [
                                    'Dar informacion personal', 'Ir de compras', 'Hablar de tu familia',
                                    'Escribir un correo electronico corto', 'seguir direcciones en un mapa'
                                                                                                        ]},
                                {id: 'fund2', name: 'fundamental plus', text: 'text', image: 'n1f2.JPG',
                                features: [
                                  'Conseguir un empleo nuevo', 'Dar una conferencia', 'Hablar con nativos',
                                  'Programar una prueba para conseguir un mejor empleo', 'leer instrucciones'
                                                                                                      ]},
                                {id: 'fund3', name: 'pre independient', text: 'text', image: 'n1f1.JPG',
                                features: [
                                  'Dar informacion personal', 'Ir de compras', 'Hablar de tu familia',
                                  'Escribir un correo electronico corto', 'seguir direcciones en un mapa'
                                                                                                      ]},
                                {id: 'fund4', name: 'independent', text: 'text', image: 'n1f1.JPG',
                                features: [
                                  'Dar informacion personal', 'Ir de compras', 'Hablar de tu familia',
                                  'Escribir un correo electronico corto', 'seguir direcciones en un mapa'
                                                                                                      ]}
                              ]},
  {name: 'Nivel 5 al 8', id: 'nivel1', modules: [
                              {id: 'fund1', name: 'fundamentos 1 Avanzado', text: 'text', image: 'n1f1.JPG',
                                features: [
                                  'Dar informacion personal', 'Ir de compras', 'Hablar de tu familia',
                                  'Escribir un correo electronico corto', 'seguir direcciones en un mapa',
                                  'hacer la reserva en un Hotel','Hablar con alguien de algun viaje que realizaste'
                                                                                                      ]},
                              {id: 'fund2', name: 'fundamentos 2', text: 'text', image: 'n1f1.JPG'},
                              {id: 'fund3', name: 'fundamentos 3', text: 'text', image: 'n1f1.JPG'},
                              {id: 'fund4', name: 'fundamentos 4', text: 'text', image: 'n1f1.JPG'}
  ]},
  {name: 'Nivel 9 al 13', id: 'nivel1', modules: [
    {id: 'fund1', name: 'fundamental', text: 'text', image: 'n1f1.JPG',
      features: [
        'Dar informacion personal', 'Ir de compras', 'Hablar de tu familia',
        'Escribir un correo electronico corto', 'seguir direcciones en un mapa'
                                                                            ]},
    {id: 'fund2', name: 'fundamental plus', text: 'text', image: 'n1f2.JPG',
    features: [
      'Conseguir un empleo nuevo', 'Dar una conferencia', 'Hablar con nativos',
      'Programar una prueba para conseguir un mejor empleo', 'leer instrucciones'
                                                                          ]},
    {id: 'fund3', name: 'pre independient', text: 'text', image: 'n1f1.JPG',
    features: [
      'Dar informacion personal', 'Ir de compras', 'Hablar de tu familia',
      'Escribir un correo electronico corto', 'seguir direcciones en un mapa'
                                                                          ]},
    {id: 'fund4', name: 'independent', text: 'text', image: 'n1f1.JPG',
    features: [
      'Dar informacion personal', 'Ir de compras', 'Hablar de tu familia',
      'Escribir un correo electronico corto', 'seguir direcciones en un mapa'
                                                                          ]}
  ]}
  ]
;
  constructor() { }

  ngOnInit() {
    console.log(this.nevels);
  }

}
