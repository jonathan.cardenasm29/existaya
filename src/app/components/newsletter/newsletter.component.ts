import { Component, OnInit } from '@angular/core';
import { SituationService } from 'src/app/services/situation.service';
import { Departament } from 'src/app/interfaces/departament';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-newsletter',
  templateUrl: './newsletter.component.html',
  styleUrls: ['./newsletter.component.css']
})
export class NewsletterComponent implements OnInit {
  profileForm = new FormGroup({
    fullName: new FormControl('', Validators.required),
    phone: new FormControl(''),
    email: new FormControl(''),
    birthday: new FormControl(''),
    departament: new FormControl(''),
    city: new FormControl(''),
    policy: new FormControl(''),
    terms: new FormControl('')
  });
  departaments: Departament[];
  cities: any[];
  selectedValue;
  constructor(private situation: SituationService) { }

  ngOnInit() {
    this.getDataDepartament();
  }
  getDataDepartament() {
    this.situation.getDepartaments()
    .subscribe((data: Departament[]) => this.departaments = data);
  }
  getDataCities(departament) {
    this.situation.getCitys(this.profileForm.value.departament)
    .subscribe((data: any[]) => this.cities = data);
  }
  saveData() {
    // [(ngModel)]="selectedValue"
    console.log(this.profileForm);
  }
  get fullName() { return this.profileForm.get('fullName'); }
}
