import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class SituationService {
  UrlRegion = 'assets/region.json';
  constructor(private http: HttpClient) { }
  region: object = {

  };
  getDepartaments() {
    return this.http.get(this.UrlRegion);
  }
  getCitys(departament: string) {
    return this.http.get('https://restcountries.eu/rest/v2/region/' + departament);
  }
}
