import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NevelsComponent } from './nevels.component';

describe('NevelsComponent', () => {
  let component: NevelsComponent;
  let fixture: ComponentFixture<NevelsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NevelsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NevelsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
